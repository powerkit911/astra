# Astra test task

Minimal requirements
------------
Vagrant: https://www.vagrantup.com/downloads

Ansible: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

VirtualBox: https://www.virtualbox.org/wiki/Linux_Downloads


Quick Start
------------
```
git clone https://gitlab.com/powerkit911/astra.git
cd astra
vagrant up
```
The default user & password is : admin

**Dont forget download vagrant box**  https://app.vagrantup.com/generic/  & edit Vargantfile, 
by default set in this project ubuntu2004.


Set some parametres like a version of _docker images, ports & etc:_ **see playbook.yml**


Tests
------------
Tested on Vagrant 2.2.19

Bugs 
------------
I faced with DNS resolv bug in vagrant box. === >  https://github.com/vagrant-libvirt/vagrant-libvirt/issues/1380

Then I add special ansible task for solve this.

If you want, you can add any DNS server for VM, see playbook.yml.

At default I set yandex DNS.



